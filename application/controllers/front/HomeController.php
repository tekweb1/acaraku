<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

	public function index()
	{
	
		$data['kegiatan'] = $this->db->get('tb_kegiatan')->result_array();
    

		$this->load->view('front/index',$data);
	}
	public function daftar()
	{
	

		$data = [
			'id_kegiatan' => $this->input->post('id_kegiatan'),
			'nama' => $this->input->post('nama'),
			'asal' => $this->input->post('asal'),
			'jk' => $this->input->post('jk'),
			'hp' => $this->input->post('hp'),
	];

$cek =	$this->db->insert('tb_peserta', $data);

if($cek){
	redirect('peserta/sukses');
}

	}


	public function sukses(){
		$this->load->view('front/sukses');
	}
}
