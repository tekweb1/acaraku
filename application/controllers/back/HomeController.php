<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

	public function index()
	{

		$data['kegiatan'] = $this->db->get('tb_kegiatan')->result_array();
    

		$this->load->view('back/_partial/master');
		$this->load->view('back/index',$data);
	}


	
	public function tambah()
	{

		
		$this->load->view('back/_partial/master');
		$this->load->view('back/tambahKegiatan');
	}
	public function daftar()
	{

		$data['kegiatan'] = $this->db->get('tb_kegiatan')->result_array();
		$this->load->view('back/_partial/master');
		$this->load->view('back/daftarKegiatan',$data);
	}
	public function detail($id)
	{

		$data['kegiatan'] = $this->db->get_where('tb_kegiatan',['id'=>$id])->row();
		$data['peserta'] = $this->db->get_where('tb_peserta',['id_kegiatan'=>$id])->result_array();


		$this->load->view('back/_partial/master');
		$this->load->view('back/detailKegiatan',$data);
	}

	public function tambahKegiatan()
	{
	
		$data = [
			'nama' => $this->input->post('nama'),
			'tanggal' => $this->input->post('tanggal'),
			'tempat' => $this->input->post('tempat'),
			'jumlah' => $this->input->post('jumlah'),
	];

	$cek =$this->db->insert('tb_kegiatan', $data);
	if($cek){
		redirect('dashboard');
	}

	}


	public function konfirmasi(){
		$id=$this->input->post('id');
			
		$this->db->set('status', 1);
		$this->db->where('id', $id);
		$this->db->update('tb_peserta');


		echo json_encode($id);
		}
	
}
