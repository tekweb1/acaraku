
      <div id="main" class="layout-navbar">
        <header class="mb-3">
          <nav class="navbar navbar-expand navbar-light navbar-top">
            <div class="container-fluid">
              <a href="#" class="burger-btn d-block">
                <i class="bi bi-justify fs-3"></i>
              </a>

              <button
                class="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto mb-lg-0">
                  <li class="nav-item dropdown me-1">
                    <a class="nav-link active dropdown-toggle text-gray-600" href="#" data-bs-toggle="dropdown" aria-expanded="false">
                      <i class="bi bi-envelope bi-sub fs-4"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton">
                      <li>
                        <h6 class="dropdown-header">Mail</h6>
                      </li>
                      <li><a class="dropdown-item" href="#">No new mail</a></li>
                    </ul>
                  </li>
                  <li class="nav-item dropdown me-3">
                    <a class="nav-link active dropdown-toggle text-gray-600" href="#" data-bs-toggle="dropdown" data-bs-display="static" aria-expanded="false">
                      <i class="bi bi-bell bi-sub fs-4"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end notification-dropdown" aria-labelledby="dropdownMenuButton">
                      <li class="dropdown-header">
                        <h6>Notifications</h6>
                      </li>
                      <li class="dropdown-item notification-item">
                        <a class="d-flex align-items-center" href="#">
                          <div class="notification-icon bg-primary">
                            <i class="bi bi-cart-check"></i>
                          </div>
                          <div class="notification-text ms-4">
                            <p class="notification-title font-bold">Successfully check out</p>
                            <p class="notification-subtitle font-thin text-sm">Order ID #256</p>
                          </div>
                        </a>
                      </li>
                      <li class="dropdown-item notification-item">
                        <a class="d-flex align-items-center" href="#">
                          <div class="notification-icon bg-success">
                            <i class="bi bi-file-earmark-check"></i>
                          </div>
                          <div class="notification-text ms-4">
                            <p class="notification-title font-bold">Homework submitted</p>
                            <p class="notification-subtitle font-thin text-sm">Algebra math homework</p>
                          </div>
                        </a>
                      </li>
                      <li>
                        <p class="text-center py-2 mb-0">
                          <a href="#">See all notification</a>
                        </p>
                      </li>
                    </ul>
                  </li>
                </ul>
                <div class="dropdown">
                  <a href="#" data-bs-toggle="dropdown" aria-expanded="false">
                    <div class="user-menu d-flex">
                      <div class="user-name text-end me-3">
                        <h6 class="mb-0 text-gray-600">John Ducky</h6>
                        <p class="mb-0 text-sm text-gray-600">Administrator</p>
                      </div>
                      <div class="user-img d-flex align-items-center">
                        <div class="avatar avatar-md">
                          <img src="<?= base_url() ?>assets/images/faces/1.jpg" />
                        </div>
                      </div>
                    </div>
                  </a>
                  <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuButton" style="min-width: 11rem">
                    <li>
                      <h6 class="dropdown-header">Hello, John!</h6>
                    </li>
                    <li>
                      <a class="dropdown-item" href="#"><i class="icon-mid bi bi-person me-2"></i> My Profile</a>
                    </li>
                    <li>
                      <a class="dropdown-item" href="#"><i class="icon-mid bi bi-gear me-2"></i> Settings</a>
                    </li>
                    <li>
                      <a class="dropdown-item" href="#"><i class="icon-mid bi bi-wallet me-2"></i> Wallet</a>
                    </li>
                    <li>
                      <hr class="dropdown-divider" />
                    </li>
                    <li>
                      <a class="dropdown-item" href="#"><i class="icon-mid bi bi-box-arrow-left me-2"></i> Logout</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </nav>
        </header>
        <div id="main-content">
          <div class="page-heading">
            <div class="page-title">
              <div class="row">
                <div class="col-12 col-md-6">
                  <nav aria-label="breadcrumb" class="breadcrumb-header">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item">
                        <a href="index.html">Dashbord</a>
                      </li>
                      <li class="breadcrumb-item active" aria-current="page">Tambah Kegiatan</li>
                    </ol>
                  </nav>
                </div>
              </div>
            </div>
            <section id="basic-horizontal-layouts">
              <div class="row match-height">
                <div class="col-md-6 col-12">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title">AGENDA KEGIATAN</h4>
                    </div>
                    <div class="card-content">
                      <div class="card-body">
                        <form class="form form-horizontal" action="<?= base_url('tambah')?>" method="post">
                          <div class="form-body">
                            <div class="row">
                              <div class="col-md-4">
                                <label>Nama Kegiatan</label>
                              </div>
                              <div class="col-md-8 form-group">
                                <input type="text" id="nama" class="form-control" name="nama" placeholder="nama kegiatan" />
                              </div>
                              <div class="col-md-4">
                                <label>Tanggal</label>
                              </div>
                              <div class="col-md-8 form-group">
                                <input type="tex" id="tanggal" class="form-control" name="tanggal" placeholder="Tanggal" />
                              </div>
                              <div class="col-md-4">
                                <label>Tempat</label>
                              </div>
                              <div class="col-md-8 form-group">
                                <input type="text" id="tempat" class="form-control" name="tempat" placeholder="Tempat" />
                              </div>
                              <div class="col-md-4">
                                <label>Jumlah Peserta</label>
                              </div>
                              <div class="col-md-8 form-group"><input type="number" id="jumlah" class="form-control" name="jumlah" placeholder="jumlah Peserta" /></div>

                              <div class="col-sm-12 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary me-1 mb-1">Simpan</button>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>

          <footer>
            <div class="footer clearfix mb-0 text-muted">
              <div class="float-start">
                <p>&copy; 2021</p>
              </div>
              <div class="float-end">
                <p>PROJECT PENGMBANGAN PERANGKAT LUNAK</p>
              </div>
            </div>
          </footer>
        </div>
      </div>
    </div>
    <script src="<?= base_url() ?>assets/js/bootstrap.js"></script>
    <script src="<?= base_url() ?>assets/js/app.js"></script>
  </body>
</html>
