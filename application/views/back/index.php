

      <div id="main">
        <header class="mb-3">
          <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
          </a>
        </header>

        <div class="page-heading">
          <div class="page-title">
            <div class="row">
              <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Daftar Kegiatan</h3>
                <p class="text-subtitle text-muted">Bootstrap’s cards provide a flexible and extensible content container with multiple variants and options.</p>
              </div>
              <div class="col-12 col-md-6 order-md-2 order-first">
                <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Card</li>
                  </ol>
                </nav>
              </div>
            </div>
          </div>
          <!-- Basic card section start -->
          <section id="content-types">
            <div class="row">
              <?php foreach($kegiatan as $ke): ?>
              <div class="col-xl-4 col-md-6 col-sm-12">
                <div class="card">
                  <div class="card-content">
                    <div class="card-body">
                      <h5 class="card-title"><?=$ke['nama']?></h5>
                       </div>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item"><?=$ke['tanggal']?></li>
                    <li class="list-group-item"><?=$ke['tempat']?></li>
                    <li class="list-group-item"><?=$ke['jumlah']?></li>
                  </ul>
                </div>
              </div>
              <?php endforeach; ?>
            </div>
          </section>
          <!-- Basic Card types section end -->
        </div>
       
      </div>
    </div>
    <script src="<?= base_url() ?>assets/js/bootstrap.js"></script>
    <script src="<?= base_url() ?>assets/js/app.js"></script>

    <script src="<?= base_url() ?>assets/extensions/jquery/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.js"></script>
    <script src="<?= base_url() ?>assets/js/pages/datatables.js"></script>
  </body>
</html>
