
      <div id="main">
        <header class="mb-3">
          <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
          </a>
        </header>

        <div class="page-heading">
          <div class="page-title">
            <div class="row">
              <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>DAFTAR PESERTA</h3>
              </div>
            </div>
          </div>
          <!-- <div class="row match-height">
            <div class="col-md-6 col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">AGENDA KEGIATAN</h4>
                </div>
                <div class="card-content">
                  <div class="card-body">
                    <form class="form form-horizontal">
                      <div class="form-body">
                        <div class="row">
                          <div class="col-md-4">
                            <label>Nama Kegiatan</label>
                          </div>
                          <div class="col-md-8 form-group">
                            <input type="text" id="namakegiatan" class="form-control" name="namakegiatan" placeholder="nama kegiatan" />
                          </div>
                          <div class="col-md-4">
                            <label>Tanggal</label>
                          </div>
                          <div class="col-md-8 form-group">
                            <input type="tex" id="tanggal" class="form-control" name="tanggal" placeholder="Tanggal" />
                          </div>
                          <div class="col-md-4">
                            <label>Tempat</label>
                          </div>
                          <div class="col-md-8 form-group">
                            <input type="text" id="tempat" class="form-control" name="tempat" placeholder="Tempat" />
                          </div>
                          <div class="col-md-4">
                            <label>Jumlah Peserta</label>
                          </div>
                          <div class="col-md-8 form-group"><input type="number" id="jumlah" class="form-control" name="jumlah" placeholder="jumlah Peserta" /></div>

                          <div class="col-sm-12 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary me-1 mb-1">Simpan</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div> -->

          <!-- Basic Tables start -->
          <section class="section">
            <div class="card">
              <div class="card-header">NAMA KEGIATAN :<b> <?=$kegiatan->nama?></b></div>
              <div class="card-body">
                <table class="table" id="table1">
                  <thead>
                    <tr>
                      <th>Nama</th>
                      <th>Jenis Kelamin</th>
                      <th>Asal</th>
                      <th>No Hp</th>
                      <th>Keterangan</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($peserta as $peserta): ?>
                    <tr>
                      <td><?= $peserta['nama']?></td>
                      <td><?= $peserta['jk']?></td>
                      <td><?= $peserta['asal']?></td>
                      <td><?= $peserta['hp']?>/td>

                      <td>
                       <?= $peserta['status'] ? '<span class="badge bg-success">HADIR</span>' : '-'    ?> 
                        <!-- <span class="badge bg-success">-</span> -->
                        <!-- <span class="badge bg-success">hadir</span> -->
                      </td>

                      <td>
                      <?php if(!$peserta['status']):   ?> 
                        <button onclick="archiveFunction()" id="konfirmasi" data-id="<?= $peserta['id']?>"  
                        class="btn btn-success">Konfirmasi</button>
                  <?php else: ?>
                 sudah di konfirmasi
                        <?php endif;?>
                    
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </section>
          <!-- Basic Tables end -->
        </div>

       
      </div>
    </div>
    <script src="<?= base_url() ?>assets/js/bootstrap.js"></script>
    <script src="<?= base_url() ?>assets/js/app.js"></script>

    <script src="<?= base_url() ?>assets/extensions/jquery/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.js"></script>
    <script src="<?= base_url() ?>assets/js/pages/datatables.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script> 
    
    <script>

      $(document).ready(function() {
            $(document).on('click', '#konfirmasi', function(e) {
                var id = $(this).data('id');
                archiveFunction(id);
                e.preventDefault();
            });
        });
 
        function archiveFunction(id) {
            Swal.fire({
                title: 'KONFIMASI KEHADIRAN?',
                text: id,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'KONFIAMSI SEKARANG',
                showLoaderOnConfirm: true,
                preConfirm: function() {
                    return new Promise(function(resolve) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>konfirmasi",
                                type: 'POST',
                                data: 'id=' + id,
                                dataType: 'json'
                            })
                            .done(function(response) {
                                Swal.fire('BERHASIL DI INFOMASI!')
                            })
                            
                    });
                },
            });
        }


    </script>
  </body>
</html>
