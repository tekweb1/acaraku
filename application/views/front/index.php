<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>Login</title>
		<link rel="stylesheet" href="<?= base_url()?>assets/css/main/app.css" />
		<link
			rel="shortcut icon"
			href="<?= base_url()?>assets/images/logo/favicon.svg"
			type="image/x-icon"
		/>
		<link
			rel="shortcut icon"
			href="<?= base_url()?>assets/images/logo/favicon.png"
			type="image/png"
		/>
	</head>

	<body>
		<nav class="navbar navbar-light">
			<div class="container d-block">
				<a class="navbar-brand ms-4" href="index.html">
					FROM PENDAFTARAN KEGIATAN
				</a>
			</div>
		</nav>

		<div class="container">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<form action="<?= base_url('peserta/daftar')?>" method="post">
							<div class="col-md-6">
								<div class="form-group">
									<label>NAMA KEGIATAN :</label>
									<select
										class="form-control"
										id="id_kegiatan"
										name="id_kegiatan"
									>		
                  <option disable>
								PILIH KEGIATAN
                </option>
										<?php foreach($kegiatan as $kegiatan): ?>
										<option value="<?=$kegiatan['id']?>">
											<?=$kegiatan['nama']?>
										</option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="form-group">
									<label for="name">Nama:</label>
									<input
										type="text"
										class="form-control"
										id="nama"
										name="nama"
									/>
								</div>

								<div class="form-group">
									<label>Jenis Kelamin:</label>
									<select class="form-control" id="jk" name="jk">
										<option value="p">Laki-Laki</option>
										<option value="l">Perempuan</option>
									</select>
								</div>

								<div class="form-group">
									<label for="hp">No hp:</label>
									<input type="number" class="form-control" id="hp" name="hp" />
								</div>

								<div class="form-group">
									<label for="asal">Asal:</label>
									<textarea
										class="form-control"
										id="asal"
										name="asal"
									></textarea>
								</div>

								<div class="form-group">
									<button class="btn btn-success" type="submit">Daftar</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
